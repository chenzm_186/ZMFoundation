# ZMFoundation

[![CI Status](https://img.shields.io/travis/rattanchen/ZMFoundation.svg?style=flat)](https://travis-ci.org/rattanchen/ZMFoundation)
[![Version](https://img.shields.io/cocoapods/v/ZMFoundation.svg?style=flat)](https://cocoapods.org/pods/ZMFoundation)
[![License](https://img.shields.io/cocoapods/l/ZMFoundation.svg?style=flat)](https://cocoapods.org/pods/ZMFoundation)
[![Platform](https://img.shields.io/cocoapods/p/ZMFoundation.svg?style=flat)](https://cocoapods.org/pods/ZMFoundation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZMFoundation is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZMFoundation'
```

## Author

rattanchen, 1005332621@qq.com

## License

ZMFoundation is available under the MIT license. See the LICENSE file for more info.
